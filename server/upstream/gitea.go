package upstream

import (
	"fmt"
	"net/url"
	"path"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

const giteaAPIRepos = "/api/v1/repos/"

// TODOs:
// * own client to store token & giteaRoot
// * handle 404 -> page will show 500 atm

func giteaRawContent(targetOwner, targetRepo, ref, giteaRoot, giteaAPIToken, resource string) ([]byte, error) {
	req := fasthttp.AcquireRequest()
	baseUrl, _ := url.Parse(giteaRoot)
	apiPath, _ := baseUrl.Parse(path.Join(giteaAPIRepos, targetOwner, targetRepo, "raw", resource+"?ref="+url.QueryEscape(ref)))

	log.Debug().Msgf("giteaRawContent: trying path %s", apiPath)
	req.SetRequestURI(apiPath.String())
	req.Header.Set(fasthttp.HeaderAuthorization, giteaAPIToken)
	res := fasthttp.AcquireResponse()

	if err := getFastHTTPClient(10*time.Second).Do(req, res); err != nil {
		return nil, err
	}
	if res.StatusCode() != fasthttp.StatusOK {
		return nil, fmt.Errorf("unexpected status code '%d'", res.StatusCode())
	}
	return res.Body(), nil
}

func giteaGetRepoBranchTimestamp(giteaRoot, repoOwner, repoName, branchName, giteaAPIToken string) (time.Time, error) {
	client := getFastHTTPClient(5 * time.Second)

	req := fasthttp.AcquireRequest()
	baseUrl, _ := url.Parse(giteaRoot)
	apiPath, _ := baseUrl.Parse(path.Join(giteaAPIRepos, repoOwner, repoName, "branches", branchName))

	log.Debug().Msgf("giteaGetRepoBranchTimestamp: trying path %s", apiPath)
	req.SetRequestURI(apiPath.String())
	req.Header.Set(fasthttp.HeaderAuthorization, giteaAPIToken)
	res := fasthttp.AcquireResponse()

	if err := client.Do(req, res); err != nil {
		log.Err(err).Msg("giteaGetRepoBranchTimestamp: failed api lookup")
		return time.Time{}, err
	}
	if res.StatusCode() != fasthttp.StatusOK {
		log.Error().Msgf("unexpected status code '%d'", res.StatusCode())
		return time.Time{}, fmt.Errorf("unexpected status code '%d'", res.StatusCode())
	}
	return time.Parse(time.RFC3339, fastjson.GetString(res.Body(), "commit", "timestamp"))
}

func giteaGetRepoDefaultBranch(giteaRoot, repoOwner, repoName, giteaAPIToken string) (string, error) {
	client := getFastHTTPClient(5 * time.Second)

	req := fasthttp.AcquireRequest()
	baseUrl, _ := url.Parse(giteaRoot)
	apiPath, _ := baseUrl.Parse(path.Join(giteaRoot, giteaAPIRepos, repoOwner, repoName))

	log.Debug().Msgf("giteaGetRepoDefaultBranch: trying path %s", apiPath)
	req.SetRequestURI(apiPath.String())
	req.Header.Set(fasthttp.HeaderAuthorization, giteaAPIToken)
	res := fasthttp.AcquireResponse()

	if err := client.Do(req, res); err != nil {
		log.Err(err).Msg("giteaGetRepoDefaultBranch: failed api lookup")
		return "", err
	}
	if res.StatusCode() != fasthttp.StatusOK {
		log.Error().Msgf("unexpected status code '%d'", res.StatusCode())
		return "", fmt.Errorf("unexpected status code '%d'", res.StatusCode())
	}
	return fastjson.GetString(res.Body(), "default_branch"), nil
}
